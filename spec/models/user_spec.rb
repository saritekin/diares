# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string
#  password   :string
#  role       :integer          default("0")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should_not allow_value("blah").for(:email) }
  it { should allow_value("sfda@bfds.com").for(:email) }
  it { should validate_presence_of :password }
  it { should validate_presence_of :role }
  it { should have_many :likes }
  it { should have_many :liked_posts }



end
