# README
Diary Website with Ruby on Rails for beginners.

<!-- MarkdownTOC depth=0 autolink=true bracket=round -->

- [Installing](#installing)
- [Run Server](#run-server)
- [Run Tests](#run-tests)



<!-- /MarkdownTOC -->


## Installing
`bundle install`

`rails db:setup`

## Run Server
`rails s`

Rails application starting in development on http://localhost:3000


## Running Tests

`rspec -f p`

RSpec makes tests.





















