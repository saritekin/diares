# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string
#  password   :string
#  role       :integer          default("0")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ApplicationRecord
  has_one :profile
  has_many :posts
  has_many :comments
  has_many :likes
  has_many :liked_posts, through: :likes, source: :post
  accepts_nested_attributes_for :profile

  validates :email, format: { with: /([^x][\.\w\-]+@\w+\.\w{2,3}(?:\.?\.?\w{2,3})?)/ }
  validates :password, length: 6..20
  validates :email, :password, :role, presence: true

  enum role: [:regular, :editor, :admin]
end
